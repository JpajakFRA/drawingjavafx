package drawing;

import drawing.commands.CommandHistory;
import drawing.handlers.MouseMoveHandler;
import drawing.handlers.SelectionHandler;
import drawing.shapes.IShape;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lewandowski on 20/12/2017.
 */
public class DrawingPane extends Pane implements Iterable <IShape>, Observable {

    private SelectionHandler selectionHandler;
    private final List<IShape> shapes;
    private CommandHistory commandHistory;

    public DrawingPane() {
        clipChildren();
        shapes = new ArrayList<IShape>();
        MouseMoveHandler mouseMoveHandler = new MouseMoveHandler(this);
        selectionHandler = new SelectionHandler(this);
        commandHistory = new CommandHistory();
    }


    /**
     * Clips the children of this {@link Region} to its current size.
     * This requires attaching a change listener to the region’s layout bounds,
     * as JavaFX does not currently provide any built-in way to clip children.
     */
    void clipChildren() {
        final Rectangle outputClip = new Rectangle();
        this.setClip(outputClip);

        this.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            outputClip.setWidth(newValue.getWidth());
            outputClip.setHeight(newValue.getHeight());
        });
    }

    public void addShape(IShape shape) {
        shapes.add(shape);
        shape.addShapeToPane(this);
        notifyObservers();
    }

    public void removeShape(IShape shape) {
        shapes.remove(shape);
        shape.removeShapeFromPane(this);
        selectionHandler.removeSelectedShape(shape);
        notifyObservers();
    }

    public void clear() {
        for (IShape shape : shapes)
            shape.removeShapeFromPane(this);
        shapes.clear();
        selectionHandler.clearSelectedShape();
        notifyObservers();
    }

    @Override
    public Iterator<IShape> iterator() {
        return shapes.iterator();
    }

    private Collection<Observer> observers = new ArrayList<Observer>();

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer obs : observers)
            obs.update();
    }

    public int getNbShape() {
        if (shapes.isEmpty())
            return 0;
        else
            return shapes.size();
    }
    public List<IShape> getShapes(){return shapes;}

    public int getNbSelectedShape() {
        return selectionHandler.getSelectedShape().size();
    }

    public List<IShape> getSelection() {
        return selectionHandler.getSelectedShape();
    }

    public CommandHistory getCommandHistory() {
        return commandHistory;
    }
}
