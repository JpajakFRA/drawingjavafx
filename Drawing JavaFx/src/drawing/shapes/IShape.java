package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

public interface IShape {
    public boolean isSelected();
    public void setSelected(boolean selected);
    public boolean isOn(double x, double y);
    public void offset(double x, double y);
    public void addShapeToPane(Pane pane);
    public void removeShapeFromPane(Pane pane);
    public IShape clone() throws CloneNotSupportedException;
    public ObservableValue translateXProperty();
    public ObservableValue translateYProperty();
}
