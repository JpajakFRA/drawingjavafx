package drawing.shapes;

import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

import static java.lang.Math.abs;

public class OrthogoLineStrategy implements IEdgeStrategy {
    public void buildPath(IShape from,IShape to, Path path){
        MoveTo moveto = new MoveTo();
        moveto.xProperty().bind(from.translateXProperty());
        moveto.yProperty().bind(from.translateYProperty());

        LineTo linestart = new LineTo();

        LineTo lineV = new LineTo();

        LineTo lineEnd = new LineTo();

        lineEnd.xProperty().bind(to.translateXProperty());
        lineEnd.yProperty().bind(to.translateYProperty());

        lineV.yProperty().bind(lineEnd.yProperty());
        lineV.xProperty().bind(moveto.xProperty().add(lineEnd.xProperty()).divide(2));

        linestart.yProperty().bind(moveto.yProperty());
        linestart.xProperty().bind(moveto.xProperty().add(lineEnd.xProperty()).divide(2));

        path.getElements().add(moveto);
        path.getElements().add(linestart);
        path.getElements().add(lineV);
        path.getElements().add(lineEnd);
    }
}
