package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class Edge implements IShape {
    public IShape from;
    public IShape to;
    public Path shape;
    public IEdgeStrategy strategy;

    public Edge(IShape from,IShape to,IEdgeStrategy strategy){
        this.from = from;
        this.to = to;
        shape = new Path();
        this.strategy = strategy;

        strategy.buildPath(this.from,this.to,this.shape);
    }

    public boolean isSelected(){
        return false;
    }

    public void setSelected(boolean selected){
        if(selected)
            shape.getStyleClass().add("selected");
        else
            shape.getStyleClass().remove("selected");
    }

    public boolean isOn(double x, double y){
        return shape.getBoundsInParent().contains(x,y);
    }

    public void offset(double x, double y){}

    public void addShapeToPane(Pane pane){
        pane.getChildren().add(shape);
    }

    public void removeShapeFromPane(Pane pane){
        pane.getChildren().remove(shape);
    }

    public Edge clone() throws CloneNotSupportedException{
        Edge clone = (Edge) super.clone();
        return clone;
    }

    public ObservableValue translateXProperty(){
        return shape.translateXProperty();
    }

    public ObservableValue translateYProperty(){
        return shape.translateYProperty();
    }

    public void setEdgeStrategy(IEdgeStrategy strategy){
        this.strategy = strategy;
    }
}
