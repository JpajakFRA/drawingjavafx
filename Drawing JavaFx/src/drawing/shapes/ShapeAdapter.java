package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;


public class ShapeAdapter implements IShape,Cloneable {
    Shape shape;

    public ShapeAdapter(Shape shape){
        this.shape = shape;
    }

    public void addShapeToPane(Pane pane){
        pane.getChildren().add(shape);
    }

    public boolean isSelected() {
        return false;
    }

    public void setSelected(boolean selected){
        if(selected)
            shape.getStyleClass().add("selected");
        else
            shape.getStyleClass().remove("selected");
    }

    public boolean isOn(double x, double y){
            return shape.getBoundsInParent().contains(x,y);
    }

    public void offset(double x, double y){
        double orgTranslateX = shape == null ? 0 : shape.getTranslateX();
        double orgTranslateY = shape == null ? 0 : shape.getTranslateY();

        shape.setTranslateX(x+orgTranslateX);
        shape.setTranslateY(y+orgTranslateY);
    }

    public void removeShapeFromPane(Pane pane){
        pane.getChildren().remove(shape);
    }

    public ShapeAdapter clone() throws CloneNotSupportedException {
        ShapeAdapter clone = (ShapeAdapter)super.clone();
        if(shape instanceof Rectangle) {
            double x = ((Rectangle) shape).getX();
            double y = ((Rectangle) shape).getY();
            double width = ((Rectangle) shape).getWidth();
            double height = ((Rectangle) shape).getHeight();
            clone.shape = new Rectangle(x,y,width,height);
            clone.shape.getStyleClass().add("rectangle");
        }
        else if(shape instanceof Ellipse) {
            double centerX = ((Ellipse) shape).getCenterX();
            double centerY = ((Ellipse) shape).getCenterY();
            double radiusX = ((Ellipse) shape).getRadiusX();
            double radiusY = ((Ellipse) shape).getRadiusY();
            clone.shape = new Ellipse(centerX,centerY,radiusX,radiusY);
            clone.shape.getStyleClass().add("ellipse");
        }
        else if(shape instanceof Polygon) {
            clone.shape = new Polygon();
            ((Polygon) clone.shape).getPoints().addAll(((Polygon) shape).getPoints());
            clone.shape.getStyleClass().add("triangle");
        }
        return clone;
    }

    public ObservableValue translateXProperty(){
        double x = shapeCenterX();
        return shape.translateXProperty().add(x);
    }

    public ObservableValue translateYProperty(){
        double y = shapeCenterY();
        return shape.translateYProperty().add(y);
    }

    public double shapeCenterX(){
        Bounds bound = shape.getBoundsInParent();
        double x = bound.getWidth()/2 + bound.getMinX();
        return x;
    }

    public double shapeCenterY(){
        Bounds bound = shape.getBoundsInParent();
        double y = bound.getHeight()/2 + bound.getMinY();
        return y;
    }

}


