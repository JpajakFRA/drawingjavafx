package drawing.shapes;

import drawing.shapes.IShape;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class ShapeComposite implements IShape,Cloneable {
    public List<IShape> group = new ArrayList<>();
    private boolean isSelected;

    public ShapeComposite(List<IShape> selected){
        group.addAll(selected);
        isSelected = false;
    }

    public boolean isSelected(){
        return isSelected;
    }

    public void setSelected(boolean selected){
        for(IShape shape : group)
            shape.setSelected(selected);
    }

    public boolean isOn(double x, double y){
        for(IShape shape : group) {
            if(shape.isOn(x,y))
                return true;
        }
        return false;
    }

    public void offset(double x, double y){
        for(IShape shape : group)
            shape.offset(x,y);
    }

    public void addShapeToPane(Pane pane){
        for(IShape shape : group)
            shape.addShapeToPane(pane);
    }

    public void removeShapeFromPane(Pane pane){
        for(IShape shape : group)
            shape.removeShapeFromPane(pane);
    }

    public ShapeComposite clone() throws CloneNotSupportedException {
        return (ShapeComposite) super.clone();
    }

    public ObservableValue translateXProperty(){
        return group.get(0).translateXProperty();
    }

    public ObservableValue translateYProperty(){
        return group.get(0).translateYProperty();
    }

}
