package drawing.shapes;

import javafx.scene.shape.Path;

public interface IEdgeStrategy {
    public void buildPath(IShape from,IShape to, Path path);
}
