package drawing.shapes;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;


public class ShapeDecorator implements IShape {
    private IShape shape;
    private Text text;



    public ShapeDecorator(IShape shape,Text txt) {
        super();
        this.shape = shape;
        text = txt;
        text.translateXProperty().bind(shape.translateXProperty());
        text.translateYProperty().bind(shape.translateYProperty());
    }

    public boolean isSelected(){
        return false;
    }

    public void setSelected(boolean selected){
        shape.setSelected(selected);
    }

    public boolean isOn(double x, double y){
        return shape.isOn(x,y);
    }

    public void offset(double x, double y){
        shape.offset(x,y);
    }

    public void addShapeToPane(Pane pane){
        shape.addShapeToPane(pane);
        pane.getChildren().add(text);
    }

    public void removeShapeFromPane(Pane pane){
        pane.getChildren().remove(text);
        shape.removeShapeFromPane(pane);
    }

    public IShape clone() throws CloneNotSupportedException{
        return null;
    }

    public ObservableValue translateXProperty(){
        return shape.translateXProperty();
    }

    public ObservableValue translateYProperty(){
        return shape.translateYProperty();
    }

}
