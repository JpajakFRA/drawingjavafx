package drawing.shapes;

import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class LineStrategy implements IEdgeStrategy {
    public void buildPath(IShape from,IShape to, Path path){
        MoveTo moveto = new MoveTo();
        moveto.xProperty().bind(from.translateXProperty());
        moveto.yProperty().bind(from.translateYProperty());

        LineTo lineto = new LineTo();
        lineto.xProperty().bind(to.translateXProperty());
        lineto.yProperty().bind(to.translateYProperty());

        path.getElements().add(moveto);
        path.getElements().add(lineto);
    }
}
