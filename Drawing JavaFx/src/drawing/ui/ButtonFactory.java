package drawing.ui;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ButtonFactory {
    public static String TRIANGLE = "Triangle";
    public static String RECTANGLE = "Rectangle";
    public static String CIRCLE = "Circle";
    public static String REMOVE = "Remove";
    public static String CLEAR = "Clear";
    public static String GROUP = "Group";
    public static String DEGROUP = "Degroup";
    public static String UNDO = "Undo";
    public static String REDO = "Redo";
    public static String DUPLICATE = "Duplicate";
    public static String TEXT = "Text";
    public static String LINK = "Link";
    private static boolean ICONS_ONLY;
    private static boolean TEXT_ONLY;

    public ButtonFactory(String style){
        if(style == "Text") {
            TEXT_ONLY = true;
            ICONS_ONLY = false;
        }
        if(style == "Icons") {
            ICONS_ONLY = true;
            TEXT_ONLY = false;
        }
    }

    public Button createButton(String buttonName){
        Button button = new Button();
        if(buttonName == TRIANGLE) {
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY) {
                Image image = new Image(getClass().getResourceAsStream("./../../img/triangle.png"),25,25,true,true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == RECTANGLE){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY) {
                Image image = new Image(getClass().getResourceAsStream("./../../img/rectangle.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == CIRCLE){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY) {
                Image image = new Image(getClass().getResourceAsStream("./../../img/circle.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == REMOVE){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY) {
                Image image = new Image(getClass().getResourceAsStream("./../../img/remove.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == CLEAR){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY) {
                Image image = new Image(getClass().getResourceAsStream("./../../img/clear.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == GROUP){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/group.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == DEGROUP){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/degroup.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == UNDO){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/undo.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == REDO){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/redo.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == DUPLICATE){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/duplicate.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == TEXT){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/text.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        else if(buttonName == LINK){
            if(TEXT_ONLY)
                button.setText(buttonName);
            else if(ICONS_ONLY){
                Image image = new Image(getClass().getResourceAsStream("./../../img/link.png"), 25, 25, true, true);
                button.setGraphic(new ImageView(image));
            }
        }
        return button;
    }
}
