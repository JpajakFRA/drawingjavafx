package drawing.ui;

import drawing.DrawingPane;
import drawing.Observable;
import drawing.Observer;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class StatutBar extends HBox implements Observer {


    Label label;
    Observable o;

    public StatutBar(DrawingPane dp){
        o = dp;
        this.getStyleClass().add("statusbar");
        this.setSpacing(3.0);
        label = new Label();
        this.update();
        this.getChildren().addAll(label);
    }

    public void update() {
        label.setText(String.valueOf(o.getNbShape()) + " shapes(s) ---- " + String.valueOf(o.getNbSelectedShape() + " selected shape(s) "));
    }
}
