package drawing.ui;

import drawing.DrawingPane;
import drawing.handlers.*;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class ToolBar extends HBox {

    private Button clearButton;
    private Button rectangleButton;
    private Button circleButton;
    private Button triangleButton;
    private Button removeButton;
    private Button groupButton;
    private Button degroupButton;
    private Button undoButton;
    private Button redoButton;
    private Button duplicateButton;
    private Button textButton;
    private Button linkButton;

    public ToolBar(DrawingPane drawingPane){
        ButtonFactory btnfact = new ButtonFactory("Icons");

        clearButton = btnfact.createButton(btnfact.CLEAR);
        clearButton.addEventFilter(ActionEvent.ACTION, new ClearButtonHandler(drawingPane));

        rectangleButton = btnfact.createButton(btnfact.RECTANGLE);
        rectangleButton.addEventFilter(ActionEvent.ACTION, new RectangleButtonHandler(drawingPane));

        circleButton = btnfact.createButton(btnfact.CIRCLE);
        circleButton.addEventFilter(ActionEvent.ACTION, new EllipseButtonHandler(drawingPane));

        triangleButton = btnfact.createButton(btnfact.TRIANGLE);
        triangleButton.addEventFilter(ActionEvent.ACTION, new TriangleButtonHandler(drawingPane));

        removeButton = btnfact.createButton(btnfact.REMOVE);
        removeButton.addEventFilter(ActionEvent.ACTION,new RemoveButtonHandler(drawingPane));

        groupButton = btnfact.createButton(btnfact.GROUP);
        groupButton.addEventFilter(ActionEvent.ACTION, new GroupButtonHandler(drawingPane));

        degroupButton = btnfact.createButton(btnfact.DEGROUP);
        degroupButton.addEventFilter(ActionEvent.ACTION, new DegroupButtonHandler(drawingPane));

        undoButton = btnfact.createButton(btnfact.UNDO);
        undoButton.addEventFilter(ActionEvent.ACTION, new UndoButtonHandler(drawingPane));

        redoButton = btnfact.createButton(btnfact.REDO);
        redoButton.addEventFilter(ActionEvent.ACTION, new RedoButtonHandler(drawingPane));

        duplicateButton = btnfact.createButton(btnfact.DUPLICATE);
        duplicateButton.addEventFilter(ActionEvent.ACTION, new DuplicateButtonHandler(drawingPane));

        textButton = btnfact.createButton(btnfact.TEXT);
        textButton.addEventFilter(ActionEvent.ACTION, new TextButtonHandler(drawingPane));

        linkButton = btnfact.createButton(btnfact.LINK);
        linkButton.addEventFilter(ActionEvent.ACTION, new LinkButtonHandler(drawingPane));

        this.getChildren().addAll(clearButton, rectangleButton, circleButton,triangleButton,removeButton,groupButton,degroupButton,undoButton,redoButton,duplicateButton,textButton,linkButton);
    }
}
