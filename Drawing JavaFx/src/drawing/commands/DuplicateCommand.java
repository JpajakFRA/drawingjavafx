package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.IShape;

import java.util.ArrayList;
import java.util.List;

public class DuplicateCommand implements ICommand{
    private DrawingPane drawingPane;
    private List<IShape> selectedShape = new ArrayList<>();
    List<IShape> clonedShapes = new ArrayList<>();


    public DuplicateCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void execute() throws CloneNotSupportedException {
        selectedShape.addAll(drawingPane.getSelection());
        for(IShape shape : selectedShape)
            clonedShapes.add((IShape)shape.clone());
        for(IShape cloned : clonedShapes)
            drawingPane.addShape(cloned);
    }

    public void undo() {
        for (IShape cloned : clonedShapes) {
            drawingPane.removeShape(cloned);
        }
    }
}
