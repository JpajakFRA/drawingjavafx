package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.IShape;
import drawing.shapes.ShapeDecorator;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class TextCommand implements ICommand {
    private DrawingPane drawingPane;
    ShapeDecorator decorator;
    List<IShape> selectedList;

    public TextCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void execute() {
        selectedList = new ArrayList<>();
        selectedList.addAll(drawingPane.getSelection());
        for(IShape shape : selectedList) {
            decorator = new ShapeDecorator(shape,new Text("My Text"));
            drawingPane.removeShape(shape);
            drawingPane.addShape(decorator);
        }
    }

    public void undo() {
        drawingPane.removeShape(decorator);
        for(IShape shape : selectedList)
            drawingPane.addShape(shape);
    }
}
