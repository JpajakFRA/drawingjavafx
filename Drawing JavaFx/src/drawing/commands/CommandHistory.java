package drawing.commands;

import java.util.Stack;

public class CommandHistory {
    private Stack<ICommand> commandPile = new Stack();
    private Stack<ICommand> undoCommand = new Stack<>();

    public void exec(ICommand command) throws CloneNotSupportedException {
        commandPile.push(command);
        command.execute();
    }

    public void undo(){
        if(!commandPile.isEmpty()) {
            ICommand command = commandPile.pop();
            undoCommand.push(command);
            command.undo();
        }
    }

    public void redo() throws CloneNotSupportedException {
        if (!undoCommand.isEmpty()) {
            ICommand redoCommand = undoCommand.pop();
            commandPile.push(redoCommand);
            redoCommand.execute();
        }
    }
}
