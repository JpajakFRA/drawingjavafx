package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.IShape;

import java.util.ArrayList;
import java.util.List;

public class RemoveCommand implements ICommand {

    private DrawingPane drawingPane;
    private List<IShape> selectedShape = new ArrayList<>();


    public RemoveCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void execute() {
        selectedShape.addAll(drawingPane.getSelection());
        for (IShape shape : selectedShape)
            drawingPane.removeShape(shape);
    }

    public void undo() {
        for (IShape shape : selectedShape) {
            drawingPane.addShape(shape);
        }
    }
}
