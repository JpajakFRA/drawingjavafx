package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.IShape;

import java.util.ArrayList;
import java.util.List;


public class ClearCommand implements ICommand {

    private DrawingPane drawingPane;
    private List<IShape> shapes = new ArrayList<>();


    public ClearCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void execute() {
        shapes.addAll(drawingPane.getShapes());
        drawingPane.clear();
    }

    public void undo() {
        for (IShape shape : shapes) {
                drawingPane.addShape(shape);
        }
    }
}
