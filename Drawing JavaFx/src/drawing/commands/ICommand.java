package drawing.commands;

public interface ICommand {
    void execute() throws CloneNotSupportedException;
    void undo();
}
