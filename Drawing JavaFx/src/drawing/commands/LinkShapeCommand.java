package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.Edge;
import drawing.shapes.IShape;
import drawing.shapes.LineStrategy;
import drawing.shapes.OrthogoLineStrategy;

import java.util.ArrayList;
import java.util.List;

public class LinkShapeCommand implements ICommand {
    Edge edge;
    private DrawingPane drawingPane;


    public LinkShapeCommand(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void execute() throws CloneNotSupportedException {
        List<IShape> selectedShape = drawingPane.getSelection();
        if(selectedShape.size() == 2){
            Edge edge = new Edge(selectedShape.get(0),selectedShape.get(1),new OrthogoLineStrategy());
            drawingPane.addShape(edge);
        }
    }

    public void undo() {
            drawingPane.removeShape(edge);
    }
}
