package drawing.commands;

import drawing.DrawingPane;
import drawing.shapes.IShape;

public class ShapeCommand implements ICommand {

    private DrawingPane drawingPane;
    private IShape shape;

    public ShapeCommand(DrawingPane drawingPane,IShape shape) {
        this.drawingPane = drawingPane;
        this.shape = shape;
    }

    public void execute() {
        drawingPane.addShape(shape);
    }

    public void undo() {
        drawingPane.removeShape(shape);
    }
}
