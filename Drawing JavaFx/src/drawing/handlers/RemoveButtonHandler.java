package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.RemoveCommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;




public class RemoveButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    private ICommand removeCommand;

    public RemoveButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        removeCommand = new RemoveCommand(drawingPane);
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.exec(removeCommand);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
