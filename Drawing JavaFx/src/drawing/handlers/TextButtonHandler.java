package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.TextCommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;




public class TextButtonHandler implements EventHandler<ActionEvent> {

    private ICommand TextCommand;
    private DrawingPane drawingPane;

    public TextButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event){
        TextCommand = new TextCommand(drawingPane);
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.exec(TextCommand);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

}
