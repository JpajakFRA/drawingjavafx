package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.CommandHistory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RedoButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;

    public RedoButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.redo();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}