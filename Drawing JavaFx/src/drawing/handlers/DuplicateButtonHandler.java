package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.CommandHistory;
import drawing.commands.DuplicateCommand;
import drawing.commands.ICommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class DuplicateButtonHandler implements EventHandler<ActionEvent> {

    private ICommand duplicateCommand;
    private DrawingPane drawingPane;

    public DuplicateButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }
    @Override
    public void handle(ActionEvent event) {
        duplicateCommand = new DuplicateCommand(drawingPane);
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.exec(duplicateCommand);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
