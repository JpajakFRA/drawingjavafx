package drawing.handlers;

import drawing.DrawingPane;
import drawing.shapes.IShape;
import drawing.shapes.ShapeComposite;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;
import java.util.List;


public class GroupButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;
    public GroupButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void handle(ActionEvent event){
        ShapeComposite composite = new ShapeComposite(drawingPane.getSelection());

        List<IShape> selected = new ArrayList<>();
        selected.addAll(drawingPane.getSelection());

        for(IShape shape : selected)
            drawingPane.removeShape(shape);

        drawingPane.addShape(composite);
    }
}
