package drawing.handlers;

import drawing.DrawingPane;
import drawing.shapes.IShape;
import drawing.shapes.ShapeAdapter;
import javafx.scene.shape.Polygon;

public class TriangleButtonHandler extends ShapeButtonHandler {

    public TriangleButtonHandler(DrawingPane drawingPane) {
        super(drawingPane);
    }

    @Override
    protected IShape createShape() {
        double width = Math.abs((destinationX - originX) / 2);
        double height;
        if(destinationY-originY < 15 || destinationX-originX < 15)
            height = 20;
        else
            height = Math.abs((destinationY-originY)*2);
        Polygon triangle= new Polygon();
        triangle.getPoints().addAll(originX,originY,destinationX,destinationY,originX+width,originY+height);
        triangle.getStyleClass().add("triangle");
        ShapeAdapter sh = new ShapeAdapter(triangle);
        return sh;
    }
}

