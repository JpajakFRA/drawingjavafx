package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.CommandHistory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class UndoButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;

    public UndoButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        commandHistory.undo();
    }
}
