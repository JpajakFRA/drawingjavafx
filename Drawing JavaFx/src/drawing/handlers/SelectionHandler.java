package drawing.handlers;

import drawing.DrawingPane;
import drawing.shapes.IShape;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.List;

public class SelectionHandler implements EventHandler<MouseEvent> {
    private DrawingPane drawingPane;
    private List<IShape> selectedShape = new ArrayList<IShape>();


    public SelectionHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
        drawingPane.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        drawingPane.addEventHandler(MouseEvent.MOUSE_RELEASED, this);
    }

    public void handle(MouseEvent event) {
        if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED) && !event.isShiftDown()) {
            for(IShape sh : selectedShape)
                sh.setSelected(false);
            selectedShape.clear();
            drawingPane.notifyObservers();
            for (IShape shape : drawingPane) {
                if (shape.isOn(event.getX(), event.getY())) {
                    selectedShape.add(shape);
                    shape.setSelected(true);
                    drawingPane.notifyObservers();
                    break;
                }
            }
        }
        if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED) && event.isShiftDown()) {

            for(IShape shape : drawingPane) {
                if(shape.isOn(event.getX(), event.getY()) && selectedShape.contains(shape)) {
                    selectedShape.remove(shape);
                    shape.setSelected(false);
                    drawingPane.notifyObservers();
                }
                else if(shape.isOn(event.getX(), event.getY()) && !(selectedShape.contains(shape))){
                    selectedShape.add(shape);
                    shape.setSelected(true);
                    drawingPane.notifyObservers();
                }
            }
        }
    }

    public List<IShape> getSelectedShape(){
        return selectedShape;
    }

    public void removeSelectedShape(IShape shape){
            selectedShape.remove(shape);
            shape.setSelected(false);
    }

    public void clearSelectedShape(){
        selectedShape.clear();
    }
}

