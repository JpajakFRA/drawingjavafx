package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ClearButtonHandler implements EventHandler<ActionEvent> {

    private ICommand clearCommand;
    private DrawingPane drawingPane;

    public ClearButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    @Override
    public void handle(ActionEvent event) {
        clearCommand = new ClearCommand(drawingPane);
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.exec(clearCommand);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
