package drawing.handlers;

import drawing.DrawingPane;
import drawing.shapes.IShape;
import drawing.shapes.ShapeComposite;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

public class DegroupButtonHandler implements EventHandler<ActionEvent> {
    private DrawingPane drawingPane;


    public DegroupButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void handle(ActionEvent event) {
        List<IShape> selected = new ArrayList<>(drawingPane.getSelection());

        for (IShape shapes : selected) {
            if (shapes instanceof ShapeComposite) {
                drawingPane.removeShape(shapes);
                for (IShape shape : ((ShapeComposite) shapes).group)
                    drawingPane.addShape(shape);
            }
        }
    }
}

