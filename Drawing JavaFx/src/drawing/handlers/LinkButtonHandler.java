package drawing.handlers;

import drawing.DrawingPane;
import drawing.commands.ClearCommand;
import drawing.commands.CommandHistory;
import drawing.commands.ICommand;
import drawing.commands.LinkShapeCommand;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class LinkButtonHandler implements EventHandler<ActionEvent> {
    private ICommand linkCommand;
    private DrawingPane drawingPane;

    public LinkButtonHandler(DrawingPane drawingPane) {
        this.drawingPane = drawingPane;
    }

    public void handle(ActionEvent event) {
        linkCommand = new LinkShapeCommand(drawingPane);
        CommandHistory commandHistory = drawingPane.getCommandHistory();
        try {
            commandHistory.exec(linkCommand);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
